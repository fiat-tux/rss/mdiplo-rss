#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::Collection 'c';
use Mojo::DOM;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode);
use XML::RSS;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   $year += 1900;
my $month = sprintf("%02d", ++$mon);
my $url   = 'https://www.monde-diplomatique.fr';

my $login = $ENV{MDIPLO_LOGIN};
my $pwd   = $ENV{MDIPLO_PWD};
my $file  = $ENV{MDIPLO_RSSFILE};

die 'Please set MDIPLO_LOGIN, MDIPLO_PWD and MDIPLO_RSSFILE. Aborting.' unless ($login && $pwd && $file);

my $ua   = Mojo::UserAgent->new();
   $ua->max_redirects(3);
my $feed = XML::RSS->new(version => '2.0');
$feed->channel(
    title       => 'Le Monde diplomatique',
    link        => $url,
    description => 'Les articles du Monde diplomatique du mois courant',
    language    => 'fr',
    generator   => 'mdiplo-rss'
);

# Connection
my $res = $ua->get("$url/connexion")->result;
my $form = Mojo::DOM->new($res->body)->find('#identification_sso')->first;
my $args = {
    formulaire_action      => $form->find('input[name="formulaire_action"]')->first->attr('value'),
    formulaire_action_args => $form->find('input[name="formulaire_action_args"]')->first->attr('value'),
    retour                 => $form->find('input[name="retour"]')->first->attr('value'),
    site_distant           => $form->find('input[name="site_distant"]')->first->attr('value'),
    email                  => $login,
    mot_de_passe           => $pwd,
    session_remember       => 'non',
    valider                => 'Valider',
};
$ua->post('https://lecteurs.mondediplo.net?page=connexion_sso' => form => $args);

# Get URLs of articles
my $dom = Mojo::DOM->new($ua->get("$url/$year/$month/")->result->body);
my @articles_links = ();
$dom->find('liste .edito a')->each(sub {
    my ($e, $num) = @_;
    push @articles_links, Mojo::URL->new($url)->path($e->attr('href'))->to_string;
});
$dom->find('.liste ul li a')->each(sub {
    my ($e, $num) = @_;
    my $link = Mojo::URL->new($url)->path($e->attr('href'))->to_string;
    if ($link ne "$url/revues/$year/$month" && index($link, "$url/revues/") == -1) {
        push @articles_links, $link;
    } else {
        my $revues_dom = Mojo::DOM->new($ua->get($link)->result->body);
        $revues_dom->find('.revues ul li a')->each(sub {
            my ($f, $num) = @_;
            push @articles_links, Mojo::URL->new($url)->path($f->attr('href'))->to_string;
        });
    }
});

my %done;
# Get articles and put it in Atom feed
c(@articles_links)->each(sub {
    my ($e, $num) = @_;

    unless ($done{$e}) {
        # Avoid to refetch an URL
        $done{$e} = 1;

        $dom = Mojo::DOM->new(decode('UTF-8', $ua->get($e)->result->body))->find('#contenu')->first;
        return unless defined $dom;

        my $title       = $dom->find('.cartouche .h1.h1')->first->text;
        my $subtitle    = $dom->find('.cartouche .surtitre')->first;
        my $auteurs     = $dom->find('.cartouche .auteurs')->first;
        my $les_auteurs = $dom->find('.lesauteurs')->first;
        my $notes       = $dom->find('.notes')->first;
        my $content     = $dom->find('.texte')->first;
        if (!$content) {
            if (index($e, 'https://www.monde-diplomatique.fr/cartes') != -1) {
                $content = $dom->find('.logocarto')->first;
            }
        }

        if (index($e, 'https://www.monde-diplomatique.fr/revues/') != -1) {
            $title = sprintf('%s — Revue', $title);
        }
        if ($dom->find('.contenu-principal.type_note-de-lecture')->size) {
            $title = sprintf('%s — Note de lecture', $title);
        }
        if ($dom->find(".contenu-principal.type_analyse-d'oeuvres")->size) {
            $title = sprintf('%s — Analyse d’œuvres', $title);
        }
        if ($dom->find('.contenu-principal.type_courrier')->size) {
            $title = sprintf('%s — Courrier des lecteurs', $title);
        }

        $content->find('img')->each(sub{
            my ($f, $num) = @_;
            my $src = Mojo::URL->new($f->attr('src'));
            unless ($src->host) {
                $src->scheme('https');
                $src->host('www.monde-diplomatique.fr');
                $f->attr(src => $src->to_string);
            }
        });
        $content->find('a')->each(sub{
            my ($f, $num) = @_;
            if ($f->attr('href')) {
                my $href = Mojo::URL->new($f->attr('href'));
                unless ($href->host) {
                    $href->scheme('https');
                    $href->host('www.monde-diplomatique.fr');
                    $f->attr(href => $href->to_string);
                }
            }
        });
        $content = $content->to_string;

        my $auth_text = '';
        if ($les_auteurs) {
            $auth_text = $les_auteurs->all_text;
        } elsif ($auteurs) {
            $auth_text = $auteurs->all_text;
        }
        if ($title =~ m/Le travail ne paie pas/i) {
            say decode('UTF-8', sprintf("%s, par %s", $title, $auth_text));
            say decode('UTF-8', $les_auteurs->all_text);
            say decode('UTF-8', $auteurs->all_text);
            say decode('UTF-8', $les_auteurs);
            say decode('UTF-8', $auteurs);
        }

        my $description = $content;
        if ($subtitle) {
            if ($auth_text) {
                $description = sprintf("<h2><i>%s</i></h2>\n<p>Par %s</p>\n%s", $subtitle, $auth_text, $content);
            } else {
                $description = sprintf("<h2><i>%s</i></h2>\n%s", $subtitle->text, $content);
            }
        } elsif ($auth_text) {
           $description = sprintf("<p>Par %s</p>\n%s", $auth_text, $content);
        }
        #$description .= sprintf("\n%s", $les_auteurs->to_string)                                         if $les_auteurs;
        $description .= sprintf("\n%s", $notes->to_string) if $notes;
        $description =~ s/\s+\n//g;
        $description =~ s/\t/ /g;
        $description =~ s/  +/ /g;

        my %hash = (
            title       => $title,
            link        => $e,
            description => $description,
            permaLink   => $e,
        );
        $feed->add_item(%hash);
    }
});

$feed->save($file);
