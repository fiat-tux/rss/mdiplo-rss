# Générateur de flux RSS pour le Monde diplomatique

Le flux RSS reprend uniquement les articles du numéro du mois.

La génération du flux nécessite un compte et un [abonnement valide](https://www.monde-diplomatique.fr/abo).

## Dépendances

```
apt install libmojolicious-perl libxml-rss-perl
```

## Configuration

La configuration est gérée par trois variables d’environnement :
- `MDIPLO_RSSFILE` : chemin du fichier RSS à créer
- `MDIPLO_LOGIN` : l’email utilisé pour votre compte du Monde diplomatique
- `MDIPLO_PWD` : le mot de passe de votre compte du Monde diplomatique

## Installation

```
wget https://framagit.org/fiat-tux/rss/mdiplo-rss/-/raw/main/mdiplo-rss.pl -O /opt/mdiplo-rss.pl
chmod +x /opt/mdiplo-rss.pl
```

## Utilisation

```
MDIPLO_RSSFILE="/var/www/mon_flux_rss_du_monde_diplo.rss" MDIPLO_LOGIN="me@exemple.org" MDIPLO_PWD="foobarbaz" /opt/mdiplo-rss.pl
```

## Cron

Voici un exemple de tâche cron pour mettre à jour le flux tous les cinquièmes jours du mois :
```
45 9 5 * * MDIPLO_RSSFILE="/var/www/mon_flux_rss_du_monde_diplo.rss" MDIPLO_LOGIN="me@exemple.org" MDIPLO_PWD="foobarbaz" /opt/mdiplo-rss.pl
```

## Licence

Affero GPLv3. Voir le fichier [LICENSE](LICENSE).
